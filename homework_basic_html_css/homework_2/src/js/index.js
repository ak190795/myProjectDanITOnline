const menu = document.querySelector('.menu')
const burger = document.querySelector('.burger')

burger.addEventListener('click', function () {
    menu.classList.toggle('active');

    if (menu.classList.contains('active')) {
        burger.setAttribute('src','./dist/img/menu-buttonX.svg')
    } else {
        burger.setAttribute('src','./dist/img/menu-button.svg')
    }
})
