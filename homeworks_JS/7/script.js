const array = ['hello', 'world', 23, '23', null]

function filterBy(array, filterValue) {
    return  array.filter(element => typeof element !== filterValue)
}

console.log(filterBy(array, 'string'))
