// 1. Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// 2. Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// 3. Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// 4. Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

const ENTER_NAME_MESSAGE = 'Enter your name'
const ENTER_AGE_MESSAGE = 'Enter you age'
const NOT_ALLOWED_MESSAGE = 'You are not allowed to visit this website'
let userName = prompt(ENTER_NAME_MESSAGE)
let initialAge = prompt(ENTER_AGE_MESSAGE)
let parsedAge = parseInt(initialAge)

if (!isNaN(parsedAge) && parsedAge !== 0 && parsedAge >= 18 && parsedAge !== null && userName !== null && userName.trim() !== '') {
    if (parsedAge >= 18 && parsedAge <= 22) {
        let bool = confirm('Are you sure you want to continue?')
        if (bool === true) {
            alert(`Welcome ${userName}`)
        } else {
            alert(NOT_ALLOWED_MESSAGE)
        }
    } else {
        alert(`Welcome ${userName}`)
    }
} else {
    if (parsedAge < 18) {
        alert(NOT_ALLOWED_MESSAGE)
    } else {
        prompt(ENTER_NAME_MESSAGE, userName)
        prompt(ENTER_AGE_MESSAGE, initialAge)
    }
}
