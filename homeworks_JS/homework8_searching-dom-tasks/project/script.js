function changeColor () {
    let elements = document.getElementsByTagName("p");
    console.log(elements);

    for (let i = 0; i < elements.length; i++) {
        elements[i].style.color = "#ff0000"
    }
}
changeColor();

let optionsList = document.getElementById("optionsList")
console.log(optionsList);

let parentNode = optionsList.parentNode;
console.log(parentNode)

let childNode = optionsList.childNodes;
console.log(childNode);

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

function changeClass () {
    let searchMainHeader = document.querySelector(".main-header");

    let searchMainHeaderLi = searchMainHeader.getElementsByTagName("li");
    console.log(searchMainHeaderLi)

    for (let i = 0; i < searchMainHeaderLi.length; i++) {
        searchMainHeaderLi[i].className = "nav-item"
    }
}
changeClass ()


