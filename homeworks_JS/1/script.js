// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

let admin
let name = "Андрей"
admin = name
console.log(admin);

// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

let days = 5
let oneDaySec = 86400
let result = days * oneDaySec
console.log(result + " секунд в 5 днях")

// Запитайте у користувача якесь значення і виведіть його в консоль.

let enterNumber = prompt("Введите число")
let showResult = enterNumber
console.log(showResult);
