const firstNumMessage = 'enter first num'
const correctFirstNumMessage = 'enter correct first num'
const secondNumMessage = 'enter second num'
const correctSecondNumMessage = 'enter correct second num'

function userInput(enterMessage, enterCorrectMessage) {
    let userNumber = prompt(enterMessage);
    while (isNaN(+userNumber) || +userNumber === 0) {
        userNumber = prompt(enterCorrectMessage, userNumber);
    }
    return parseFloat(userNumber)
}

let firstNum = userInput(firstNumMessage, correctFirstNumMessage)
let secondNum = userInput(secondNumMessage, correctSecondNumMessage)
let operation = prompt('enter operation');
while (operation !== "*" && operation !== "+" && operation !== "-" && operation !== "/") {
    operation = prompt('enter correct operation', operation);
}

function performMathAction(firstNum, secondNum, operation) {
    let result;
    switch (operation) {
        case "*":
            return result = firstNum * secondNum;
        case "/":
            return result = firstNum / secondNum;
        case "+":
            return result = firstNum + secondNum;
        case "-":
            return result = firstNum - secondNum;
    }
}

console.log(performMathAction(firstNum, secondNum, operation))
