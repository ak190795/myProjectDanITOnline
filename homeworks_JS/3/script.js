// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа. Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.


let userNumber = parseInt(prompt('please enter a number'));

while (isNaN(userNumber) || userNumber <= 0 || userNumber === null) {
    userNumber = parseInt(prompt('please enter a positive number'));
}

if (userNumber >= 5) {
    for (let i = 1; i <= userNumber; i++) {
        if (i % 5 === 0) {
            console.log(i)
        }
    }
} else {
    console.log('Sorry, no numbers')
}
