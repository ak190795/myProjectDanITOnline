const icons = Array.from(document.querySelectorAll('.icon-password'))
const inputs = document.querySelectorAll('input')

icons.forEach(element => element.addEventListener('click', function () {
    if (this.classList.contains('fa-eye')) {
        this.classList.replace('fa-eye', 'fa-eye-slash')
        inputs[icons.indexOf(this)].setAttribute('type', 'text')
    } else {
        this.classList.replace('fa-eye-slash', 'fa-eye')
        inputs[icons.indexOf(this)].setAttribute('type', 'password')
    }
}))

const submitBtn = document.querySelector('button')
const span = document.createElement('span')
span.style.cssText = 'color: red;' +
    'position: absolute;' +
    'left: 0;' +
    'bottom: 10px;' +
    'font-size: 0.75em'
span.textContent = 'Passwords should match'

submitBtn.addEventListener('click', function (e) {
    e.preventDefault()
    if (inputs[0].value === inputs[1].value){
        if (span) span.remove()
        alert ('You are welcome!')
    } else {
        inputs[1].parentNode.append(span)
    }
})
