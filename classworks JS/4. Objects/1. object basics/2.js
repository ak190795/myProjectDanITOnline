/**
 * Задание 2.
 *
 * Расширить функционал объекта из предыдущего задания:
 * - Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
 * - Добавить метод, который меняет значение указанного свойства объекта.
 *
 * Продвинутая сложность:
 * Метод должен быть «умным» — он генерирует ошибку при совершении попытки
 * смены значения несуществующего в объекте свойства.
 */


 let newObject = {
    name: "Andrey",
    lastname: "Kononenko",
    job: "programist",
    sayHi : function (){
        console.log(`Привет, меня зовут ${this.name} ${this.lastname}`)
    },
    changeProperty: function(name, value){
        if (name in this) {
            this.name = value;
        } else {
            throw new Error(`Property ${name} dosnt exist`)
        }
    }
}
console.log(newObject);
newObject.sayHi();
newObject.changeProperty("job", "loh")