/**
 * Задание 1.
 *
 * Создать объект пользователя, который обладает тремя свойствами:
 * - Имя;
 * - Фамилия;
 * - Профессия.
 *
 * А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
 */

let newObject = {
    name: "Andrey",
    lastname: "Kononenko",
    job: "programist",
    sayHi : function (){
        console.log("Привет")
    }
}
console.log(newObject);
newObject.sayHi();
